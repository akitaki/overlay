EAPI=7

DESCRIPTION="Utility to open MS office files by zathura"
HOMEPAGE="https://github.com/paoloap/zaread"
SRC_URI="https://github.com/paoloap/zaread/archive/master.zip -> ${P}.zip"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=( app-office/libreoffice app-text/zathura-pdf-poppler app-text/zathura )
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack "${A}" && mv zaread-master/ "${S}"
}

src_install() {
	dobin "zaread"
	insinto "/usr/share/applications/"
	doins "${FILESDIR}/zaread.desktop"
}
